FROM node:carbon

# Create app directory

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . ./
RUN npm install
RUN npm run build

WORKDIR server
RUN npm install



EXPOSE 3000
 
CMD node server.js