var express = require('express');
var socket = require('socket.io');
const path = require('path');

var app = express();


//app.use(express.static(path.join(__dirname, '../build')));
//
//app.get('/', function (req, res) {
//    res.sendFile(path.join(__dirname, 'build', 'index.html'));
//});

const chatrooms = [
    {
        roomName: 'Technology',
            connectedUsers: []
    },
    {
        roomName: 'Art',
        connectedUsers: []
    },
    {
        roomName: 'Science',
        connectedUsers: []
    }
];



server = app.listen(8088, function(){
    console.log('server is running on port 3000')
});



io = socket(server);


io.on('connection', (socket) => {
    var addedUser = false;
    console.log('Client connected on ' + socket.id);
    console.log('emiting chatrooms to client');
    socket.emit('available rooms', chatrooms);

    socket.on('join', (chatter) => {
        //for emptry nickname
        if (!chatter.nickname || chatter.nickname === '') {
            chatter.statusOfConnection = 'emptyNickname';
        } else {
            var nickAlreadyExists = false;
            for (let chatroom of chatrooms) {
                if (chatroom.roomName === chatter.chatroom) {
                    if (chatroom.connectedUsers.indexOf(chatter.nickname) > -1) {
                        //user with same nickname already exists in the chatroom return error
                        console.log('Nickname ' + chatter.nickname + ' already exists in room');
                        chatter.statusOfConnection = 'nicknameExists';
                    } else {
                        chatroom.connectedUsers.push(chatter.nickname);
                        console.log('joining chat room:' + chatter.chatroom);
                        socket.join(chatter.chatroom);

                        console.log('emitting to chatroom ' + chatroom.roomName + ' that ' + chatter.nickname + ' has joined!');
                        chatter.statusOfConnection = 'connected';
                        io.in(chatroom.roomName).emit('new user joined', chatroom.connectedUsers);
                    }
                    break;
                }
            }
        }

        socket.emit('join result', chatter);

    });

    socket.on('leave', (chatter) => {

        var currentChatroom = chatrooms.find((chatroom) => {
            if (chatroom.roomName === chatter.chatroom)
                return chatroom;
        });

        var userIndex = currentChatroom.connectedUsers.indexOf(chatter.nickname);
        if (userIndex > -1) {
            currentChatroom.connectedUsers.splice(userIndex, 1);
            console.log(chatter.nickname + ' removed from room ' + currentChatroom.roomName);
            io.in(currentChatroom.roomName).emit('new user joined', currentChatroom.connectedUsers); //TO UPDATE USERS LIST IN CHAT WINDOWS
            socket.leave(currentChatroom.roomName);
        }
    });
    // when the client emits 'new message', this listens and executes
    socket.on('new message', function (chatter) {
        console.log('emitting to all users in chatroom ' + chatter.chatroom);
        io.in(chatter.chatroom).emit('update messages', chatter);

    });

    socket.on('re emit connected users', function (chatter) {
        if (chatter) {
            console.log('emitting to all users in chatroom ' + chatter.chatroom);
            var joinedRoom = chatrooms.find((chatroom) => {
                if (chatroom.roomName === chatter.chatroom) {
                    return chatroom;
                }
            });
            io.in(joinedRoom.roomName).emit('new user joined', joinedRoom.connectedUsers);
        }
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function () {
        console.log(' SOCKET DISCONNECTED ');
    });
});
