This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

A simple chat room service which (really, very simple ;).

Choose a nick and chatroom, and join the chat room. It's that simple!.


#RUNNING BUILT APP WITH DOCKER
1. Checkout the branch **docker_image_changes** / OR copy the **docker-compose.yml** from **docker_image_changes**
2. Use the docker-compose.yml file to start the application.
3. Go to <docker-machine-ip>:3000/ to access the application.  
_<docker_image>/server/server.js is started at port 3000. This server serves a built version of react SPA chat service, along with the necassary backend processing for chat service._  
_Docker image available @ **docker pull alishahid/a_la_slack**_

#RUNNING APP IN LOCAL DEV ENVIRONMENT
1. Please checkout the **master** branch.
2. Open two separate consoles.
3. Run the following commands in first console (to start react app in dev mode):  
	> cd <checkedout_out_master_branch>  
	> npm start  	
	**_Above commands will start server at port 3000 to serve react application at runtime in dev mode, also includes a reverse proxy to redirect all API hits at localhost:3000 to localhost:8088 (where our nodejs backend server will be running)_**
   
4. Run the following commands in second consolde (to start the nodejs server):   
	> cd <checkedout_out_master_branch>/server  
	> node server.js  
	**_Above commands will start nodejs server at port 8088. This server will perform the backend processing for the chat service._**
   
5. Go to localhost:3000/ to access the application.

