export const JOIN_CHATROOM_ERRORED = 'JOIN_CHATROOM_ERRORED',
    JOIN_CHATROOM_SUCCESS = 'JOIN_CHATROOM_SUCCESS',
    IS_LOADING = 'IS_LOADING', NEW_MESSAGE = 'NEW_MESSAGE', REFRESH_MESSAGES = 'REFRESH_MESSAGES' ,NEW_USER_JOINED = 'NEW_USER_JOINED',
    JOIN_CHATROOM_NICKNAME_EXISTS = 'JOIN_CHATROOM_NICKNAME_EXISTS', JOIN_CHATROOM_EMPTY_NICKNAME = 'JOIN_CHATROOM_EMPTY_NICKNAME', UPDATE_CHATROOMS = 'UPDATE_CHATROOMS', LEAVE_ROOM = 'LEAVE_ROOM';

export function connectedToChatroom(type, payload) {
    return {
        type: type,
        payload: payload
    }

}

export function isLoading (bool) {
    return {
        type: IS_LOADING,
        payload: bool
    }
}


export function newMessage (chatMessage) {
    return {
        type: NEW_MESSAGE,
        payload: chatMessage
    }
}

export function newUserJoined (users) {
    return {
        type: NEW_USER_JOINED,
        payload: users
    }
}

export function updateChatrooms(chatrooms) {
    return {
        type: UPDATE_CHATROOMS,
        payload: chatrooms
    }
}

export function leaveRoom(chatter) {
    return {
        type: LEAVE_ROOM,
        payload: chatter
    }
}

export function refreshMessages () {
    return {
        type: REFRESH_MESSAGES,
        payload: null
    }
}