import React, { Component } from 'react';
import './App.css';
import Chatter from './containers/chatter-container'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        <h3> Welcome to à la Slack </h3>
        </header>
          <div className="joinChatroom">
              <Chatter />
          </div>
      </div>
    );
  }
}

export default App;
