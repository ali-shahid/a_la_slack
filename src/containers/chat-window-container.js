import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import {newMessage} from '../actions/index';
import {newUserJoined} from '../actions/index';
import {leaveRoom} from '../actions/index';
import Messages from './messages-container';
import Users from './users-container';
import socket from '../socketio/socket-client';


/**
 * Chat window container
 * For rendering the chat room window
 * showing listed of connected users in the room
 * displaying messages being sent to the room
 *
 * To use socket io with thunk to handle emitting and receiving
 * of messages and changing states on them.
 *
 */
class ChatwindowContainer extends Component {

    componentDidMount() {
        this.updateMessagesListner = (chatMessage) => {
            this.props.newMessage(chatMessage);
            //call an action here to dispatch other methods
        };

        socket.on('update messages',this.updateMessagesListner );


        this.newUserJoinedListener = (users) => {
            this.props.newUserJoined(users);
        };
        socket.on('new user joined', this.newUserJoinedListener );
    }

    emitMessage(chatMessage) {
        socket.emit('new message', chatMessage);
    }

    componentWillUnmount() {
        socket.removeListener('update messages', this.updateMessagesListner);
        socket.removeListener('new user joined', this.newUserJoinedListener);
    }

    leaveRoom() {
        socket.emit('leave', this.props.chatter);
        this.props.leaveRoom(this.props.chatter);
    }

    handleKeyPress(target) {
        if (target.charCode===13) {
            this.refs.submitButton.click();
        }

    }

    renderChatwindow() {
        this.onEnterEventHandler = (event) => {
            if (event.charCode === 13) {
                this.onClickEventHandler();
            }
        };

        this.onClickEventHandler = () => {
            this.emitMessage(
                {   'nickname': this.props.chatter.nickname,
                    'message': this.refs.chatmessage.value,
                    'chatroom': this.props.chatter.chatroom
                });
            this.refs.chatmessage.value = '';
        };

        return (<div className="mainChatwindow">
            <div>
            <h3> {this.props.chatter.chatroom} </h3>
            <button ref="leaveRoom" onClick={()=>{
                this.leaveRoom();
            }}>Leave Room</button></div>
            <Users />
            <Messages />
            <div className="enterMessage">
                    <input ref="chatmessage" className="w3-input" onKeyPress={this.onEnterEventHandler}></input>
                    <button ref="submitButton" onClick={this.onClickEventHandler}>Send message</button>
            </div>
        </div>);
    }
    render() {
        if (this.props.chatter !== null) {
            return this.renderChatwindow();
        } else {
            return null;
        }

    }
}

function mapStateToProps(state) {
    return {
        chatter: state.chatter
    };

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            newMessage: newMessage,
            newUserJoined: newUserJoined,
            leaveRoom: leaveRoom
        }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatwindowContainer);