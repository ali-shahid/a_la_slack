import React, {Component} from 'react';
import {connect} from 'react-redux'


/**
 * For rendering messages based on redux store chat messages array
 *
 */
class MessagesContainer extends Component {

    renderMessages() {
        return (<ul className="chatMessagesList">{this.props.messages.map(
            (message, i) => {
                var classToApplyOnNickname = "";

                if (message.nickname === this.props.chatter.nickname) {
                    classToApplyOnNickname = "purpleNickname"
                } else {
                    if (i%2 === 0) {
                        classToApplyOnNickname = "blueNickname"
                    } else {
                        classToApplyOnNickname = "redNickname"
                    }
                }
                return (<li key={i}><label className={classToApplyOnNickname}>{message.nickname}</label>: {message.message}</li>);
            }
        )}</ul>);
    }

    render() {
        return (
            <div className="chatMessages">{this.renderMessages()}</div>
        );
    }



}

function mapStateToProps(state) {
    return {
        messages: state.messages,
        chatter: state.chatter
    };

}


export default connect(mapStateToProps)(MessagesContainer);