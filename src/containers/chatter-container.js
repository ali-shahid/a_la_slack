import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import {connectedToChatroom} from '../actions/index';
import {refreshMessages} from '../actions/index';
import {updateChatrooms, JOIN_CHATROOM_NICKNAME_EXISTS, JOIN_CHATROOM_SUCCESS} from '../actions/index';
import socket from '../socketio/socket-client'
import Chatwindow from './chat-window-container'


class ChatterContainer extends Component {

    componentDidMount() {

        this.availableChatroomsListener = (data) => {
            this.props.updateChatrooms(data);
        };
        socket.on('available rooms', this.availableChatroomsListener);
    }

    emitJoinMessageToServer(chatter) {
        socket.emit('join', chatter);
        this.props.refreshMessages();

        this.joinResultListener = (chatter) => {
            if (chatter.statusOfConnection === 'connected') {
                this.props.connectedToChatroom(JOIN_CHATROOM_SUCCESS, chatter);
            } else if (chatter.statusOfConnection === 'nicknameExists') {
                this.props.connectedToChatroom(JOIN_CHATROOM_NICKNAME_EXISTS, chatter);
            } else if (chatter.statusOfConnection === 'emptyNickname') {
                this.props.connectedToChatroom(JOIN_CHATROOM_NICKNAME_EXISTS, chatter);
            }
        };

        socket.on('join result',this.joinResultListener);
    }

    componentWillUnmount() {
        socket.removeListener('available rooms', this.availableChatroomsListener);
        socket.removeListener('join result',this.joinResultListener);
    }

    renderError() {
        if (this.props.chatter != null) {
            if (this.props.chatter.statusOfConnection === 'nicknameExists') {
                return (<div className="errorMessage">Nickname already in use! Please choose another nickname</div>);
            }

            if (this.props.chatter.statusOfConnection === 'emptyNickname') {
                return (<div className="errorMessage">Please enter a nickname!</div>);
            }

            if (this.props.chatter.statusOfConnection === 'error') {
                return (<div className="errorMessage">Something went wrong, please try again</div>);
            }
        }

        return null;

    }


    renderChatroomsDropdown() {
        return (<select ref="chatrooms">{this.props.chatrooms.map((chatroom, i) =>{
            return (<option key={i} value={chatroom.roomName}>{chatroom.roomName}</option>);
        })}</select>);
    }
    renderJoinChatroomBlock() {
        this.onEnterEventHandler = (event) => {
            if (event.charCode === 13) {
                this.onClickEventHandler();
            }
        };

        this.onClickEventHandler = () => {
            this.emitJoinMessageToServer(
                {   'nickname': this.refs.nickname.value,
                    'chatroom': this.refs.chatrooms.options[this.refs.chatrooms.selectedIndex].text,
                    'statusOfConnection': ''
                });
        };


        return (<div>
            <h2>Please choose a nickname!</h2>
            <div>
            <div><label>Nickname: </label></div><div><input style={{width: '100px'}} type="text" ref="nickname" onKeyPress={this.onEnterEventHandler} /></div>
            <br/>
            <div><label>Chatrooms: </label></div>
            <div>{this.renderChatroomsDropdown()}</div>
            <div style={{padding: '20px'}}><button onClick={this.onClickEventHandler}
            >Join Chat</button></div></div>{this.renderError()}
        </div>);
    }
    render() {
        if (this.props.chatter !== null && this.props.chatter.statusOfConnection === 'connected') {
            return (<div className="chatWindow">
                <Chatwindow />
            </div>);
            //means chatter is active
        } else {

            return (this.renderJoinChatroomBlock());
        }

    }
}

function mapStateToProps(state) {
    return {
        chatter: state.chatter,
        chatrooms: state.chatrooms
    };

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            connectedToChatroom: connectedToChatroom,
            updateChatrooms: updateChatrooms,
            refreshMessages: refreshMessages
        }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatterContainer);