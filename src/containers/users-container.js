import React, {Component} from 'react';
import {connect} from 'react-redux'
import socket from '../socketio/socket-client';
import {newUserJoined} from '../actions/index';
import {bindActionCreators} from 'redux';


/**
 * For rendering users joining/leaving chat
 *
 */
class UsersContainer extends Component {

    componentDidMount() {
        socket.emit('re emit connected users', this.props.chatter);

        this.newUserJoinedListener = (users) => {
            this.props.newUserJoined(users);
        };
        socket.on('new user joined', this.newUserJoinedListener);
    }

    componentWillUnmount() {
        socket.removeListener('new user joined', this.newUserJoinedListener);
    }

    renderUsers() {
        return this.props.users.map(
            (user, i) => {
                return (<li key={i}>{user}</li>);
            }
        );
    }

    render() {
        return (
            <div className="connectedUsers"><h3>Connected Users</h3><ul>{this.renderUsers()}</ul></div>
        );
    }
}

function mapStateToProps(state) {
    return {
        users: state.users,
        chatter: state.chatter
    };

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            newUserJoined: newUserJoined,
        }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer);