import {JOIN_CHATROOM_ERRORED, JOIN_CHATROOM_SUCCESS,JOIN_CHATROOM_NICKNAME_EXISTS, LEAVE_ROOM, JOIN_CHATROOM_EMPTY_NICKNAME} from '../actions/index';

/**
 * Either returns a chatter or return null in case an error occurred
 * @param state
 * @param action
 * @returns {*}
 */
export function connectToChatroom(state = null, action = {}) {
    switch (action.type) {
        case JOIN_CHATROOM_SUCCESS:
            //CALL API TO JOIN CHAT ROOM
            return action.payload;
        case JOIN_CHATROOM_NICKNAME_EXISTS:
            return action.payload;
        case JOIN_CHATROOM_ERRORED:
            return action.payload;
        case JOIN_CHATROOM_EMPTY_NICKNAME:
            return action.payload;
        case LEAVE_ROOM:
            return Object.assign({}, action.payload, {statusOfConnection: 'disconnected'});
        default:
            return state;

    }
}


