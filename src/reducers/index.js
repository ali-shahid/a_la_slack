import {combineReducers} from 'redux';
import {connectToChatroom} from './chatter-reducer';
import {chatMessages} from './messages-reducer'
import {updateChatrooms} from './chatrooms-reducer';
import {users} from './users-reducer';


const allReducers = combineReducers({
    chatter: connectToChatroom,
    messages: chatMessages,
    users: users,
    chatrooms: updateChatrooms
});

export default allReducers