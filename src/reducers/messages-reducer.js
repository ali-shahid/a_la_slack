import {NEW_MESSAGE, REFRESH_MESSAGES} from '../actions/index';

export function chatMessages(state = [], action = {}) {
    switch (action.type) {
        case NEW_MESSAGE:
            //Immutabitiliy with below
            return [].concat(state, action.payload);

        case REFRESH_MESSAGES:
            return [];
        default:
            return state;

    }
}



