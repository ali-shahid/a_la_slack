import {NEW_USER_JOINED} from '../actions/index';

export function users(state = [], action = {}) {
    switch (action.type) {
        case NEW_USER_JOINED:
            return [].concat(action.payload);
        default:
            return state;

    }
}



