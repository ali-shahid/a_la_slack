import {UPDATE_CHATROOMS} from '../actions/index';

export function updateChatrooms(state = [], action = {}) {
    switch (action.type) {
        case UPDATE_CHATROOMS:
            //Immutabitiliy with below
            return action.payload;
        default:
            return state;

    }
}



